#ifndef APPWINDOW_HPP
#define APPWINDOW_HPP

#include <QMainWindow>
#include <QMenuBar>
#include <QMenu>
#include <QAction>
#include <vector>
#include "Viewer.hpp"
#include "scene.hpp"

class AppWindow : public QMainWindow
{
    Q_OBJECT

public:
    AppWindow(SceneNode* sceneRoot);

private slots:
  void reset_position();
  void reset_orientation();
  void reset_joints();
  void reset_all();
  void set_mode_position();
  void set_mode_joint();
  void undo();
  void redo();
  void set_circle_option(bool checked);
  void set_frontfacecull_option(bool checked);
  void set_backfacecull_option(bool checked);
  void set_zbuffer_option(bool checked);
  void set_cool_option(bool checked);

private:
    void createActions();

    // Each menu itself
    QMenu* m_menu_app;
    QMenu* m_menu_mode;
    QMenu* m_menu_edit;
    QMenu* m_menu_options;

    Viewer* m_viewer;
    std::vector<QAction*> m_menu_actions;
};

#endif