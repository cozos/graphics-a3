#ifndef CS488_PRIMITIVE_HPP
#define CS488_PRIMITIVE_HPP

#include <QColor>
#include <QMatrix4x4>
#include "material.hpp"
#include "algebra.hpp"

class Viewer;

class Primitive {
public:
  virtual ~Primitive();
  virtual void walk_gl(bool picking) const = 0;
  void set_viewer(Viewer *viewer){m_viewer = viewer;}
  void set_material(Material *material){material = material;}
  void set_pick_color(QColor color){uniquePickingColor = color;}
  
  void set_transform(QMatrix4x4 tMatrix){
    transformMatrix = tMatrix;
  }

  void setPicked(bool picked){
    PICKED = picked;
  }

  bool PICKED;
  QColor uniquePickingColor;
  Viewer *m_viewer;
  Material *material;
  QMatrix4x4 transformMatrix;
};

class Sphere : public Primitive {
public:
  virtual ~Sphere();
  virtual void walk_gl(bool picking) const;
};

#endif
