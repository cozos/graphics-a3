#include "primitive.hpp"
#include "Viewer.hpp"

Primitive::~Primitive()
{
}

Sphere::~Sphere()
{
}

void Sphere::walk_gl(bool picking) const
{
  m_viewer->draw_sphere(transformMatrix,uniquePickingColor, picking, PICKED);
}
