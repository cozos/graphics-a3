#version 130 

in vec3 vert;

uniform vec4 light_Position;
uniform vec4 light_Ambient;
uniform vec4 light_Diffuse;
uniform vec4 light_Specular; 
uniform vec4 material_Ambient; 
uniform vec4 material_Diffuse; 
uniform vec4 material_Specular;
uniform float material_Shininess;

uniform mat3 normalMatrix;
uniform mat4 mvpMatrix;

out vec4 ADSLightIntensity;

void main()
{	
	vec3 eyeVertexNormal = normalize( normalMatrix * vert);
 	vec4 eyeCoords = mvpMatrix * vec4(vert,1.0);
	
	vec3 eyeLightSource = normalize(vec3(light_Position - eyeCoords));
 	vec3 eyeViewer = normalize(-eyeCoords.xyz);
	vec3 eyeReflect = reflect( -eyeLightSource, eyeVertexNormal );

	float lightSource_dot_vertexNormal = max( dot(eyeLightSource,eyeVertexNormal), 0.0);

	vec4 ambient = light_Ambient * material_Ambient;
	vec4 diffuse = light_Diffuse * material_Diffuse * lightSource_dot_vertexNormal;
	vec4 spec = vec4(0.0);
	
	if( lightSource_dot_vertexNormal > 0.0 )
	spec = light_Specular * material_Specular * pow( max( dot(eyeReflect,eyeViewer), 0.0 ), material_Shininess );
 
 	ADSLightIntensity = ambient + diffuse + spec;

    gl_Position = mvpMatrix * vec4(vert, 1.0);
}
