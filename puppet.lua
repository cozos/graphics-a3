--
-- CS488 -- Introduction to Computer Graphics
-- 
-- a3mark.lua
--
-- A very simple scene creating a trivial puppet.  The TAs will be
-- using this scene as part of marking your assignment.  You'll
-- probably want to make sure you get reasonable results with it!

rootnode = gr.node('root')

shininess = 0.65
specular = 0.5

red = gr.material({ 1.0, 0.0, 0.0}, {specular, specular, specular}, shininess)
blue = gr.material({ 0.0, 0.0, 1.0}, {specular, specular, specular}, shininess)
green = gr.material({ 0.0, 1.0, 0.0}, {specular, specular, specular}, shininess)
white = gr.material({ 1.0, 1.0, 1.0}, {specular, specular, specular}, shininess)
orange = gr.material({ 1.0, 0.4, 0}, {specular, specular, specular}, shininess)
taric_purple = gr.material({ 0.6, 0, 0.78}, {specular, specular, specular}, shininess)
myfavoritegreen = gr.material({ 0, 0.9, 0.4}, {specular, specular, specular}, shininess)
grey = gr.material({ 0.4, 0.4, 0.4}, {specular, specular, specular}, shininess)

torso = gr.sphere('torso')
shoulder = gr.sphere('shoulder')
left_upper_arm = gr.sphere('left_upper_arm')
left_forearm = gr.sphere('left_forearm')
left_hand = gr.sphere('left_hand')
neck = gr.sphere('neck')
head = gr.sphere('head')
nose = gr.sphere('nose')
eye1 = gr.sphere('eye1')
eye2 = gr.sphere('eye2')
chin1 = gr.sphere('chin1')
chin2 = gr.sphere('chin2')
right_upper_arm = gr.sphere('right_upper_arm')
right_forearm = gr.sphere('right_forearm')
right_hand = gr.sphere('right_hand')

hips = gr.sphere('hips')
left_thigh = gr.sphere('left_thigh')
left_calf = gr.sphere('left_calf')
left_foot = gr.sphere('left_foot')
right_thigh = gr.sphere('right_thigh')
right_calf = gr.sphere('right_calf')
right_foot = gr.sphere('right_foot')

torso:set_material(myfavoritegreen)
shoulder:set_material(blue)
left_upper_arm:set_material(grey)
left_forearm:set_material(grey)
left_hand:set_material(white)
neck:set_material(orange)
head:set_material(green)
nose:set_material(red)
eye1:set_material(white)
eye2:set_material(white)
chin1:set_material(taric_purple)
chin2:set_material(taric_purple)
right_upper_arm:set_material(grey)
right_forearm:set_material(grey)
right_hand:set_material(white)

hips:set_material(orange);
left_thigh:set_material(red);
left_calf:set_material(red);
left_foot:set_material(white);
right_thigh:set_material(red);
right_calf:set_material(red);
right_foot:set_material(white);

torso_joint = gr.joint('torso_joint',{0.0,0.0,0.0},{0.0,0.0,0.0})
shoulder_joint = gr.joint('shoulder_joint',{0.0,0.0,0.0},{0.0,0.0,0.0})
left_upper_arm_joint = gr.joint('left_upper_arm_joint',{0.0,0.0,0.0},{0.0,0.0,0.0})
left_forearm_joint = gr.joint('left_forearm_joint',{0.0,0.0,0.0},{0.0,0.0,0.0})
left_hand_joint = gr.joint('left_hand_joint',{0.0,0.0,0.0},{0.0,0.0,0.0})
neck_joint = gr.joint('neck_joint',{0.0,0.0,0.0},{0.0,0.0,0.0})
head_joint = gr.joint('head_joint',{0.0,0.0,0.0},{0.0,0.0,0.0})
face_joint = gr.joint('face_joint',{0.0,0.0,0.0},{0.0,0.0,0.0})
right_upper_arm_joint = gr.joint('right_upper_arm_joint',{0.0,0.0,0.0},{0.0,0.0,0.0})
right_forearm_joint = gr.joint('right_forearm_joint',{0.0,0.0,0.0},{0.0,0.0,0.0})
right_hand_joint = gr.joint('right_hand_joint',{0.0,0.0,0.0},{0.0,0.0,0.0})

hips_joint = gr.joint('hips_joint',{0.0,0.0,0.0},{0.0,0.0,0.0})
left_thigh_joint = gr.joint('left_thigh_joint',{0.0,0.0,0.0},{0.0,0.0,0.0})
left_calf_joint = gr.joint('left_calf_joint',{0.0,0.0,0.0},{0.0,0.0,0.0})
left_foot_joint = gr.joint('left_foot_joint',{0.0,0.0,0.0},{0.0,0.0,0.0})
right_thigh_joint = gr.joint('right_thigh_joint',{0.0,0.0,0.0},{0.0,0.0,0.0})
right_calf_joint = gr.joint('right_calf_joint',{0.0,0.0,0.0},{0.0,0.0,0.0})
right_foot_joint = gr.joint('right_foot_joint',{0.0,0.0,0.0},{0.0,0.0,0.0})

rootnode:add_child(torso_joint)

torso_joint:add_child(shoulder_joint)
shoulder_joint:add_child(left_upper_arm_joint)
left_upper_arm_joint:add_child(left_forearm_joint)
left_forearm_joint:add_child(left_hand_joint)
shoulder_joint:add_child(neck_joint)
neck_joint:add_child(head_joint)
head_joint:add_child(face_joint)
shoulder_joint:add_child(right_upper_arm_joint)
right_upper_arm_joint:add_child(right_forearm_joint)
right_forearm_joint:add_child(right_hand_joint)

torso_joint:add_child(hips_joint)
hips_joint:add_child(left_thigh_joint)
left_thigh_joint:add_child(left_calf_joint)
left_calf_joint:add_child(left_foot_joint)
hips_joint:add_child(right_thigh_joint)
right_thigh_joint:add_child(right_calf_joint)
right_calf_joint:add_child(right_foot_joint)

torso_joint:add_child(torso)
shoulder_joint:add_child(shoulder)
left_upper_arm_joint:add_child(left_upper_arm)
left_forearm_joint:add_child(left_forearm)
left_hand_joint:add_child(left_hand)
neck_joint:add_child(neck)
head_joint:add_child(head)
face_joint:add_child(nose)
face_joint:add_child(eye1)
face_joint:add_child(eye2)
face_joint:add_child(chin1)
face_joint:add_child(chin2)
right_upper_arm_joint:add_child(right_upper_arm)
right_forearm_joint:add_child(right_forearm)
right_hand_joint:add_child(right_hand)

hips_joint:add_child(hips)
left_thigh_joint:add_child(left_thigh)
left_calf_joint:add_child(left_calf)
left_foot_joint:add_child(left_foot)
right_thigh_joint:add_child(right_thigh)
right_calf_joint:add_child(right_calf)
right_foot_joint:add_child(right_foot)

rootnode:translate(0,1.2,0)

shoulder_joint:translate(0,2.3,0);

neck_joint:translate(0,0.7,0)
neck:translate(0,0,-0.1)
head_joint:translate(0,1.1,0)

face_joint:translate(0,-0.15,1);
eye1:translate(-1,3,0)
eye2:translate(1,3,0)
nose:translate(0,0,0)
chin1:translate(-0.5,-4.2, -1)
chin2:translate(0.5,-4.2,-1)

left_upper_arm_joint:translate(-3,-0.8,0);
right_upper_arm_joint:translate(3,-0.8,0);

left_forearm_joint:translate(0,-1.7,0);
left_hand_joint:translate(0,-1.1,0);
right_forearm_joint:translate(0,-1.7,0);
right_hand_joint:translate(0,-1.1,0);

hips_joint:translate(0,-2.9,0);

left_thigh_joint:translate(-1,-0.9, -0.3);
right_thigh_joint:translate(1,-0.9, -0.3);

left_calf_joint:translate(0,-0.8,0);
left_foot_joint:translate(0,-0.8,0);
right_calf_joint:translate(0,-0.8,0);
right_foot_joint:translate(0,-0.8,0);

torso:scale(2.5,2.5,2)
shoulder:scale(3.5,0.3,1)
neck:scale(0.3,0.45,0.4)
face_joint:scale(0.15,0.15,0.15);
nose:scale(1,1,4)
left_upper_arm_joint:scale(0.2,1.3,1);
right_upper_arm_joint:scale(0.2,1.3,1);
right_hand:scale(1,0.3,0.6)
left_hand:scale(1,0.3,0.6)
hips_joint:scale(1.6,1,1.5);
left_thigh_joint:scale(0.3,1,1);
right_thigh_joint:scale(0.3,1,1);
left_calf_joint:scale(0.8, 2,0.7);
right_calf_joint:scale(0.8, 2,0.7);
left_foot_joint:scale(1, 0.3, 2);
right_foot_joint:scale(1, 0.3, 2);


-- s0 = gr.sphere('s0')
-- rootnode:add_child(s0)
-- s0:set_material(white)

-- s1 = gr.sphere('s1')
-- rootnode:add_child(s1)
-- s1:scale(0.1, 2.0, 0.1)
-- s1:set_material(red)

-- s2 = gr.sphere('s2')
-- rootnode:add_child(s2)
-- s2:translate(2.0, -2.0, 0.0)
-- s2:rotate('z', -90.0)
-- s2:scale(0.1, 2.0, 0.1)
-- s2:set_material(blue)

-- s3 = gr.sphere('s3')
-- rootnode:add_child(s3)
-- s3:scale(0.1, 0.1, 2.0)
-- s3:translate(0.0, -20.0, 1.0)
-- s3:set_material(green)

-- s4 = gr.joint('s4',{0.0,0.0,0.0},{0.0,0.0,0.0})

-- rootnode:translate(-0.75, 0.25, -5.0)
-- rootnode:rotate('y', -20.0)


return rootnode