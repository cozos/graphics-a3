#include "material.hpp"
#include <QColor>
#include "Viewer.hpp"

Material::~Material()
{
}

PhongMaterial::PhongMaterial(const Colour& kd, const Colour& ks, double shininess)
  : m_kd(kd), m_ks(ks), m_shininess(shininess)
{
}

PhongMaterial::~PhongMaterial()
{
}

void PhongMaterial::set_viewer(Viewer *viewer){
	m_viewer = viewer;
}

void PhongMaterial::apply_gl() const
{
	QColor q_material_diffuse = QColor(m_kd.R() * 255.0, m_kd.G() * 255.0, m_kd.B() * 255.0);
	QColor q_material_specular = QColor(m_ks.R() * 255.0, m_ks.G() * 255.0, m_ks.B() * 255.0);
	float shininess = (float)m_shininess;

	m_viewer->setup_sphere(q_material_diffuse, q_material_specular, shininess);
}


